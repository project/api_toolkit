<?php

/**
 * @file
 * Provides install hook implementations for the API Toolkit Examples module.
 */

/**
 * Implements hook_install().
 */
function api_toolkit_examples_install() {
  $storage = \Drupal::entityTypeManager()
    ->getStorage('node');

  $examplePage = $storage->create([
    'type' => 'example_page',
    'title' => 'An example page',
    'field_example_page_link' => ['uri' => 'https://example.com'],
  ]);
  $examplePage->save();

  $anotherExamplePage = $storage->create([
    'type' => 'example_page',
    'title' => 'Another example page',
    'field_example_page_link' => ['uri' => 'https://another.example.com'],
  ]);
  $anotherExamplePage->save();

  $config = \Drupal::configFactory()
    ->getEditable('api_toolkit.settings');
  $formats = $config->get('route_formats') ?? [];
  $formats = array_merge($formats, ['api_toolkit_examples_json']);
  $config->set('route_formats', $formats);
  $config->save();
}

/**
 * Implements hook_uninstall().
 */
function api_toolkit_examples_uninstall() {
  $storage = \Drupal::entityTypeManager()
    ->getStorage('node');
  $examplePages = $storage->loadByProperties(['type' => 'example_page']);
  $storage->delete($examplePages);

  $storage = \Drupal::entityTypeManager()
    ->getStorage('node_type');
  $type = $storage->load('example_page');
  $type->delete();

  $config = \Drupal::configFactory()
    ->getEditable('api_toolkit.settings');
  $formats = $config->get('route_formats') ?? [];
  $formats = array_diff($formats, ['api_toolkit_examples_json']);
  $config->set('route_formats', $formats);
  $config->save();
}
