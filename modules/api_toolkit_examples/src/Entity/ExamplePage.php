<?php

namespace Drupal\api_toolkit_examples\Entity;

use Drupal\link\LinkItemInterface;
use Drupal\node\Entity\Node;

/**
 * Entity bundle class for the "Example Page" content type.
 */
class ExamplePage extends Node {

  /**
   * Get the similar pages.
   *
   * @return ExamplePage[]
   *   The similar pages.
   */
  public function getSimilarPages(): array {
    return $this->get('field_example_page_similar')->referencedEntities();
  }

  /**
   * Set similar pages.
   *
   * @param static[] $entities
   *   The similar pages.
   *
   * @return static
   */
  public function setSimilarPages(array $entities) {
    return $this->set('field_example_page_similar', $entities);
  }

  /**
   * Get the link field item.
   */
  public function getLink(): ?LinkItemInterface {
    return $this->get('field_example_page_link')->first();
  }

  /**
   * Set the link field item.
   *
   * @return static
   */
  public function setLink(string $uri, ?string $title = NULL) {
    return $this->set('field_example_page_link', [
      'uri' => $uri,
      'title' => $title,
    ]);
  }

}
