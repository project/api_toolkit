<?php

namespace Drupal\api_toolkit_examples\Normalizer;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\link\LinkItemInterface;
use Drupal\serialization\Normalizer\NormalizerBase;

/**
 * Normalizes a link field item to an array with all its fields.
 */
class LinkItemNormalizer extends NormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $format = ['api_toolkit_examples'];

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = [LinkItemInterface::class];

  /**
   * Normalizes a link field item to an array.
   *
   * @param \Drupal\link\LinkItemInterface $object
   *   The link field item to normalize.
   * @param string $format
   *   Format the normalization result will be encoded as.
   * @param array{cacheability: CacheableMetadata|null} $context
   *   Context options for the normalizer.
   */
  public function normalize($object, $format = NULL, array $context = []): array {
    $context['cacheability'] ??= new CacheableMetadata();

    $url = $object->getUrl()->toString(TRUE);
    $context['cacheability']->addCacheableDependency($url);

    return [
      'url' => $url->getGeneratedUrl(),
      'title' => $object->title,
    ];
  }

}
