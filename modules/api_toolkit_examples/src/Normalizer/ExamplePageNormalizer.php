<?php

namespace Drupal\api_toolkit_examples\Normalizer;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\api_toolkit_examples\Entity\ExamplePage;
use Drupal\serialization\Normalizer\NormalizerBase;

/**
 * Normalizes an example page node to an array with all its fields.
 */
class ExamplePageNormalizer extends NormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $format = ['api_toolkit_examples'];

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = [ExamplePage::class];

  /**
   * Normalizes an example page node to an array.
   *
   * @param \Drupal\api_toolkit_examples\Entity\ExamplePage $object
   *   The example page node to normalize.
   * @param string $format
   *   Format the normalization result will be encoded as.
   * @param array{cacheability: CacheableMetadata|null} $context
   *   Context options for the normalizer.
   */
  public function normalize($object, $format = NULL, array $context = []): array {
    $context['cacheability'] ??= new CacheableMetadata();
    $context['cacheability']->addCacheableDependency($object);

    return [
      'id' => $object->id(),
      'title' => $object->getTitle(),
      'created' => $object->getCreatedTime(),
      'author' => $this->serializer->normalize($object->getOwner(), 'api_toolkit_examples_simple', $context),
      'link' => $this->serializer->normalize($object->getLink(), $format, $context),
      'similarPages' => $this->serializer->normalize($object->getSimilarPages(), 'api_toolkit_examples_simple', $context),
    ];
  }

}
