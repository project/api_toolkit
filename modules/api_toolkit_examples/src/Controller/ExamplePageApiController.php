<?php

namespace Drupal\api_toolkit_examples\Controller;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\api_toolkit\Exception\ApiValidationException;
use Drupal\api_toolkit\Response\CacheableJsonResponse;
use Drupal\api_toolkit\Response\CacheablePagedJsonResponse;
use Drupal\api_toolkit\Response\JsonResponse;
use Drupal\api_toolkit_examples\Entity\ExamplePage;
use Drupal\api_toolkit_examples\Request\CreateOrUpdateExamplePageRequest;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Controller for CRUD API endpoints for example pages.
 */
class ExamplePageApiController implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected PagerManagerInterface $pagerManager;

  /**
   * The normalizer service.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface
   */
  protected NormalizerInterface $normalizer;

  /**
   * The validator service.
   *
   * @var \Symfony\Component\Validator\Validator\ValidatorInterface
   */
  protected ValidatorInterface $validator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->pagerManager = $container->get('pager.manager');
    $instance->normalizer = $container->get('serializer');
    $instance->validator = $container->get('api_toolkit.validator');

    return $instance;
  }

  /**
   * Get a single example page.
   */
  public function get(ExamplePage $examplePage): CacheableJsonResponse {
    $cacheability = new CacheableMetadata();
    $data = $this->normalizer->normalize($examplePage, 'api_toolkit_examples', ['cacheability' => $cacheability]);
    $response = CacheableJsonResponse::createWithData($data);

    $response->getCacheableMetadata()
      ->addCacheableDependency($cacheability);

    return $response;
  }

  /**
   * Get a list of all example pages.
   */
  public function all(): CacheableJsonResponse {
    $examplePages = $this->entityTypeManager->getStorage('node')
      ->loadByProperties(['type' => 'example_page']);
    $examplePages = array_values($examplePages);

    $cacheability = new CacheableMetadata();
    $data = $this->normalizer->normalize($examplePages, 'api_toolkit_examples', ['cacheability' => $cacheability]);
    $response = CacheableJsonResponse::createWithData($data);

    $response->getCacheableMetadata()
      ->addCacheTags(['node_list:example_page'])
      ->addCacheableDependency($cacheability);

    return $response;
  }

  /**
   * Get a paged list of example pages.
   */
  public function paged(): CacheablePagedJsonResponse {
    $storage = $this->entityTypeManager->getStorage('node');
    $ids = $storage->getQuery()
      ->condition('type', 'example_page')
      ->pager(1)
      ->accessCheck(TRUE)
      ->execute();

    $examplePages = [];
    if ($ids !== []) {
      $examplePages = $storage->loadMultiple($ids);
      $examplePages = array_values($examplePages);
    }

    $cacheability = new CacheableMetadata();
    $data = $this->normalizer->normalize($examplePages, 'api_toolkit_examples', ['cacheability' => $cacheability]);
    $pager = $this->pagerManager->getPager();
    $response = CacheablePagedJsonResponse::createWithPager($data, $pager);

    $response->getCacheableMetadata()
      ->addCacheTags(['node_list:example_page'])
      ->addCacheableDependency($cacheability);

    return $response;
  }

  /**
   * Create a new example page.
   */
  public function post(CreateOrUpdateExamplePageRequest $request): JsonResponse {
    $violations = $this->validator->validate($request, NULL, [
      'create',
      'Default',
    ]);

    if ($violations->count() > 0) {
      throw ApiValidationException::create($violations);
    }

    $examplePage = ExamplePage::create();
    $this->updateExamplePage($examplePage, $request);
    $examplePage->save();

    return JsonResponse::createWithData($this->normalizer->normalize($examplePage, 'api_toolkit_examples'));
  }

  /**
   * Update an existing example page.
   */
  public function patch(ExamplePage $examplePage, CreateOrUpdateExamplePageRequest $request): JsonResponse {
    $violations = $this->validator->validate($request, NULL, [
      'update',
      'Default',
    ]);

    if ($violations->count() > 0) {
      throw ApiValidationException::create($violations);
    }

    $this->updateExamplePage($examplePage, $request);
    $examplePage->save();

    return JsonResponse::createWithData($this->normalizer->normalize($examplePage, 'api_toolkit_examples'));
  }

  /**
   * Delete an existing example page.
   */
  public function delete(ExamplePage $examplePage): JsonResponse {
    $examplePage->delete();

    return JsonResponse::create(NULL, Response::HTTP_NO_CONTENT);
  }

  /**
   * Update an example page from a request.
   */
  protected function updateExamplePage(ExamplePage $examplePage, CreateOrUpdateExamplePageRequest $request): void {
    if ($request->title) {
      $examplePage->setTitle($request->title);
    }

    if ($request->similarPages) {
      $examplePage->setSimilarPages($request->similarPages);
    }

    if ($request->link) {
      $examplePage->setLink($request->link);
    }
  }

}
