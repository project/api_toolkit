<?php

namespace Drupal\api_toolkit_examples\Request;

use Drupal\api_toolkit\Plugin\Validation\Constraint\EntityExists;
use Drupal\api_toolkit\Request\ApiRequestBase;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Request class for creating or updating an example page.
 *
 * @see \Drupal\api_toolkit_examples\Controller\ExamplePageApiController::post()
 * @see \Drupal\api_toolkit_examples\Controller\ExamplePageApiController::patch()
 */
class CreateOrUpdateExamplePageRequest extends ApiRequestBase {

  /**
   * The example page title.
   *
   * This property is required when creating the entity, not when updating it.
   * Its max length is 255 characters due to database constraints on the
   * node_field_data.title column.
   *
   * @var string
   */
  #[Assert\NotBlank(groups: ['create'])]
  #[Assert\Length(max: 255)]
  public ?string $title = NULL;

  /**
   * The example page link.
   *
   * This property is always optional.
   * Its max length is 2048 characters due to database constraints on the
   * node__field_example_page_link_uri.field_example_page_link_uri column.
   *
   * @var string
   */
  #[Assert\Length(min: 0, max: 2048)]
  #[Assert\Url]
  public ?string $link = NULL;

  /**
   * Pages similar to the example page.
   *
   * This property is always optional.
   * It accepts an array of node IDs, which are validated to exist in the
   * database and to be of the correct type.
   *
   * @var array
   */
  #[Assert\All([
    new EntityExists(entityTypeId: 'node', bundle: 'example_page', fieldName: 'nid'),
  ])]
  public array $similarPages = [];

}
