<?php

namespace Drupal\api_toolkit;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Populate the CachedNormalizer with registered normalizers and encoders.
 */
class RegisterSerializationClassesCompilerPass implements CompilerPassInterface {

  /**
   * Adds services to the CachedNormalizer.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
   *   The container to process.
   */
  public function process(ContainerBuilder $container) {
    $serializerDefinition = $container->getDefinition('serializer');
    $apiToolkitDefinition = $container->getDefinition('api_toolkit.cached_normalizer');

    // Get the registered normalizers and encoders from the serializer.
    $apiToolkitDefinition->replaceArgument(3, $serializerDefinition->getArgument(0));
    $apiToolkitDefinition->replaceArgument(4, $serializerDefinition->getArgument(1));
  }

}
