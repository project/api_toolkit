<?php

namespace Drupal\api_toolkit\Request;

/**
 * An interface for domain-specific request classes.
 */
interface ApiRequestInterface {

  /**
   * Get an array with all properties of the given object.
   *
   * We use get_object_vars because uninitialized (not provided) properties are
   * missing from this array. This way, we can differentiate between
   * uninitialized and null values.
   */
  public function all(): array;

  /**
   * Checks whether a property exists and is initialized.
   */
  public function has(string $key): bool;

}
