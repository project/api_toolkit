<?php

namespace Drupal\api_toolkit\Request;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyTrait;

/**
 * Base class for API requests.
 */
abstract class ApiRequestBase implements ApiRequestInterface, RefinableCacheableDependencyInterface {

  use RefinableCacheableDependencyTrait;

  /**
   * {@inheritdoc}
   */
  public function all(): array {
    return get_object_vars($this);
  }

  /**
   * {@inheritdoc}
   */
  public function has(string $key): bool {
    return array_key_exists($key, $this->all());
  }

  /**
   * Add a cache context to the request object.
   *
   * This is required by ObjectNormalizer in order to
   * add a cache context to the protected property.
   *
   * @param string $cacheContext
   *   The cache context to be added.
   *
   * @return $this
   *   Return self for chaining.
   *
   * @see \Symfony\Component\Serializer\Normalizer\ObjectNormalizer
   */
  public function addCacheContext(string $cacheContext) {
    return $this->addCacheContexts([$cacheContext]);
  }

  /**
   * Remove a cache context from the request object.
   *
   * This is required by ObjectNormalizer in order to
   * remove a cache context from the protected property.
   *
   * @param string $cacheContext
   *   The cache context to be removed.
   *
   * @return $this
   *   Return self for chaining.
   *
   * @see \Symfony\Component\Serializer\Normalizer\ObjectNormalizer
   */
  public function removeCacheContext(string $cacheContext) {
    $key = array_search($cacheContext, $this->cacheContexts, TRUE);
    if ($key !== FALSE) {
      unset($this->cacheContexts[$key]);
    }
    return $this;
  }

  /**
   * Add a cache tag to the request object.
   *
   * This is required by ObjectNormalizer in order to
   * add a cache tag to the protected property.
   *
   * @param string $cacheTag
   *   The cache tag to be added.
   *
   * @return $this
   *   Return self for chaining.
   *
   * @see \Symfony\Component\Serializer\Normalizer\ObjectNormalizer
   */
  public function addCacheTag(string $cacheTag) {
    return $this->addCacheTags([$cacheTag]);
  }

  /**
   * Remove a cache tag from the request object.
   *
   * This is required by ObjectNormalizer in order to
   * remove a cache tag from the protected property.
   *
   * @param string $cacheTag
   *   The cache tag to be removed.
   *
   * @return $this
   *   Return statement.
   *
   * @see \Symfony\Component\Serializer\Normalizer\ObjectNormalizer
   */
  public function removeCacheTag(string $cacheTag) {
    $key = array_search($cacheTag, $this->cacheTags, TRUE);
    if ($key !== FALSE) {
      unset($this->cacheTags[$key]);
    }
    return $this;
  }

}
