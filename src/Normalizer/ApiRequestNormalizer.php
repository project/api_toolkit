<?php

namespace Drupal\api_toolkit\Normalizer;

use Drupal\api_toolkit\Exception\ApiValidationException;
use Drupal\api_toolkit\Plugin\Validation\Constraint\Enum;
use Drupal\api_toolkit\Request\ApiRequestBase;
use Drupal\api_toolkit\Request\ApiRequestInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Denormalizes classes implementing ApiRequestInterface.
 *
 * @see \Drupal\api_toolkit\Request\ApiRequestInterface
 * @see \Drupal\api_toolkit\ArgumentResolver\ApiRequestResolver
 */
class ApiRequestNormalizer implements NormalizerInterface, DenormalizerInterface {

  /**
   * A string map with values that should always be mapped to other values.
   *
   * @var string[]
   */
  const VALUE_MAP = [
    '' => NULL,
    'null' => NULL,
    'true' => TRUE,
    'false' => FALSE,
    '[]' => NULL,
  ];

  /**
   * A map from possible type declarations to types returned by gettype().
   *
   * @var string[]
   */
  const TYPE_MAP = [
    'integer' => 'int',
    'double' => 'float',
    'boolean' => 'bool',
    'array' => 'array',
    'string' => 'string',
  ];

  /**
   * The denormalizer.
   *
   * @var \Symfony\Component\Serializer\Normalizer\DenormalizerInterface
   */
  protected $denormalizer;

  /**
   * The validator.
   *
   * @var \Symfony\Component\Validator\Validator\ValidatorInterface
   */
  protected $validator;

  /**
   * Constructs a new ApiRequestNormalizer.
   *
   * @param \Symfony\Component\Serializer\Normalizer\DenormalizerInterface $denormalizer
   *   The denormalizer.
   * @param \Symfony\Component\Validator\Validator\ValidatorInterface $validator
   *   The validator.
   */
  public function __construct(
    DenormalizerInterface $denormalizer,
    ValidatorInterface $validator,
  ) {
    $this->denormalizer = $denormalizer;
    $this->validator = $validator;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize(mixed $data, ?string $format = NULL, array $context = []): array|string|int|float|bool|\ArrayObject|null {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization(mixed $data, ?string $format = NULL, array $context = []): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function denormalize(mixed $data, string $type, ?string $format = NULL, array $context = []): mixed {
    // Collect all values.
    if ($data instanceof Request) {
      $values = $this->getValues($data);
    }
    else {
      $values = $data;
    }

    // Collect all request-specific properties.
    // Skip properties from the base class.
    $reflection = new \ReflectionClass($type);
    $properties = array_filter(
        $reflection->getProperties(),
        function ($property) {
          return $property->getDeclaringClass()->getName() !== ApiRequestBase::class;
        }
    );

    // Transform values.
    $originalValues = $values;

    foreach ($properties as $property) {
      $name = $property->getName();

      if (!isset($values[$name])) {
        continue;
      }

      // Transform values to their proper scalar types.
      $value = &$values[$name];

      if (is_string($value) && array_key_exists($value, self::VALUE_MAP)) {
        $value = self::VALUE_MAP[$value];
      }

      if (is_array($value)) {
        foreach ($value as $subKey => $subValue) {
          if (is_string($subValue) && array_key_exists($subValue, self::VALUE_MAP)) {
            $value[$subKey] = self::VALUE_MAP[$subValue];
          }
        }
      }

      $propertyType = $property->getType();

      if ($propertyType instanceof \ReflectionNamedType) {
        if (enum_exists($propertyType->getName()) && method_exists($propertyType->getName(), 'from') && (is_string($value) || is_int($value))) {
          if ($enum = $propertyType->getName()::tryFrom($value)) {
            $value = $enum;
          }
        }

        if ($propertyType->getName() === 'array' && $value === NULL) {
          $value = [];
        }

        if ($propertyType->getName() === 'string' && !$propertyType->allowsNull() && $value === NULL) {
          $value = '';
        }

        if ($propertyType->getName() === 'int' && is_numeric($value)) {
          $value = (int) $value;
        }

        if ($propertyType->getName() === 'float' && is_numeric($value)) {
          $value = (float) $value;
        }
      }

      if (($propertyType instanceof \ReflectionUnionType) || $type instanceof \ReflectionIntersectionType) {
        foreach ($propertyType->getTypes() as $singleType) {
          if ($value === NULL && $singleType === 'array') {
            $value = [];
          }
        }
      }

      unset($value);
    }

    // Check whether the values are allowed to be assigned to their properties.
    // @todo Use DenormalizerInterface::COLLECT_DENORMALIZATION_ERRORS once Drupal supports Symfony 5.4
    // @see https://symfony.com/blog/new-in-symfony-5-4-serializer-improvements#collect-denormalization-type-errors
    $violations = new ConstraintViolationList();

    foreach ($properties as $property) {
      $name = $property->getName();
      $propertyType = $property->getType();

      if (!$propertyType->allowsNull() && !$property->hasDefaultValue()) {
        $notNullViolations = $this->validator->validate($values[$name] ?? NULL, [new NotNull()]);

        foreach ($notNullViolations as $notNullViolation) {
          $this->addViolation($violations, NULL, $name, NULL, NULL, NULL, $notNullViolation);
        }

        if ($notNullViolations->count() > 0) {
          continue;
        }
      }

      $value = $values[$name] ?? NULL;
      $valueType = self::TYPE_MAP[gettype($value)] ?? gettype($value);

      if ($propertyType instanceof \ReflectionNamedType) {
        if (enum_exists($propertyType->getName()) && !$value instanceof \BackedEnum) {
          $enumViolations = $this->validator->validate($value, [new Enum(['enum' => $propertyType->getName()])]);
          foreach ($enumViolations as $enumViolation) {
            $this->addViolation($violations, NULL, $name, NULL, NULL, NULL, $enumViolation);
          }
        }

        if (in_array($propertyType->getName(), self::TYPE_MAP, TRUE) && $propertyType->getName() !== $valueType && $valueType !== 'NULL') {
          $this->addTypeViolation($propertyType, $property->getName(), $originalValues[$name], $value, $violations);
        }
      }

      if ($propertyType instanceof \ReflectionUnionType) {
        $hasMatch = FALSE;
        foreach ($propertyType->getTypes() as $singleType) {
          if (in_array($singleType->getName(), self::TYPE_MAP, TRUE) && $singleType->getName() === $valueType) {
            $hasMatch = TRUE;
            break;
          }
        }
        if (!$hasMatch) {
          $this->addTypeViolation($propertyType->getTypes()[0], $property->getName(), $originalValues[$name], $value, $violations);
        }
      }
    }

    if ($violations->count() > 0) {
      throw ApiValidationException::create($violations);
    }

    // Add cacheability metadata.
    foreach ($properties as $property) {
      $name = $property->getName();

      // Every property might come from a query param,
      // so add the right cache contexts.
      $values['cacheContexts'][] = 'url.query_args:' . $name;
    }

    return $this->denormalizer->denormalize($values, $type, $format, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function supportsDenormalization(mixed $data, string $type, ?string $format = NULL, array $context = []): bool {
    return (is_array($data) || $data instanceof Request)
      && is_a($type, ApiRequestInterface::class, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTypes(?string $format): array {
    return ['object' => TRUE];
  }

  /**
   * Extract values from the request.
   */
  protected function getValues(Request $request): array {
    $values = array_merge(
      $request->request->all(),
      $request->query->all(),
    );

    // Add non-internal attributes.
    foreach ($request->attributes->all() as $key => $value) {
      if (strpos($key, '_') === 0) {
        continue;
      }

      $values[$key] = $value;
    }

    if ($request->getContentTypeFormat() === 'json' && $content = $request->getContent()) {
      try {
        $content = json_decode($content, TRUE, 512, JSON_THROW_ON_ERROR);
        $values = array_merge($values, $content);
      }
      catch (\JsonException $exception) {
        throw ApiValidationException::create(
          NULL,
          Response::HTTP_BAD_REQUEST,
          sprintf('Error while parsing json: %s', $exception->getMessage()),
          $exception
        );
      }
    }

    return $values;
  }

  /**
   * Create a new type violation and add it to the list.
   */
  protected function addTypeViolation(\ReflectionType $propertyType, string $name, $originalValue, $value, ConstraintViolationListInterface $violations): void {
    $typeName = $propertyType->getName();
    $constraint = new Type([
      'type' => self::TYPE_MAP[$typeName] ?? $typeName,
    ]);
    $message = str_replace('{{ type }}', $typeName, $constraint->message);

    $this->addViolation($violations, $message, $name, $value, $originalValue, $constraint);
  }

  /**
   * Create a new violation and add it to the list.
   */
  protected function addViolation(
    ConstraintViolationListInterface $violations,
    ?string $message = NULL,
    ?string $name = NULL,
    $value = NULL,
    $originalValue = NULL,
    ?Constraint $constraint = NULL,
    ?ConstraintViolation $violation = NULL,
  ): void {
    if ($violation instanceof ConstraintViolation) {
      $violation = new ConstraintViolation(
        $violation->getMessage() ?? $message,
        $violation->getMessageTemplate(),
        $violation->getParameters(),
        $violation->getRoot() ?? $originalValue ?? $value,
        $violation->getPropertyPath() ?: $name,
        $violation->getInvalidValue() ?? $value,
        $violation->getPlural(),
        $violation->getCode(),
        $constraint ?? $violation->getConstraint(),
      );
    }
    else {
      $violation = new ConstraintViolation(
        $message,
        NULL,
        [],
        $originalValue ?? $value,
        $name,
        $value,
        NULL,
        NULL,
        $constraint
      );
    }

    $violations->add($violation);
  }

}
