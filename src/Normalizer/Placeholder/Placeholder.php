<?php

namespace Drupal\api_toolkit\Normalizer\Placeholder;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;

/**
 * A value placeholder containing a callback that will be invoked at runtime.
 */
class Placeholder {

  use DependencySerializationTrait;

  /**
   * The callback to invoke.
   *
   * @var mixed
   */
  protected mixed $callback = NULL;

  /**
   * The object, in case the callback is in format [$object, 'method'].
   *
   * @var object|null
   */
  protected ?object $callbackObject = NULL;

  /**
   * The method, in case the callback is in format [$object, 'method'].
   *
   * @var string|null
   */
  protected ?string $callbackMethod = NULL;

  /**
   * The arguments to pass to the callback.
   *
   * @var array
   */
  public array $arguments = [];

  /**
   * Constructs the placeholder object.
   *
   * @param mixed $callback
   *   The callback to invoke.
   * @param array $arguments
   *   The arguments to pass to the callback.
   */
  public function __construct(mixed $callback, array $arguments = []) {
    // If the callback contains a possible service, store it in a property to
    // make sure it's picked up by DependencySerializationTrait.
    if (is_array($callback) && count($callback) === 2 && is_object($callback[0]) && is_string($callback[1])) {
      $this->callbackObject = $callback[0];
      $this->callbackMethod = $callback[1];
    }
    else {
      $this->callback = $callback;
    }

    $this->arguments = $arguments;
  }

  /**
   * Invokes the callback.
   *
   * @return mixed
   *   The return value of the callback.
   */
  public function __invoke(): mixed {
    if ($this->callbackObject && $this->callbackMethod) {
      $callback = [$this->callbackObject, $this->callbackMethod];
    }
    else {
      $callback = $this->callback;
    }

    return call_user_func_array($callback, $this->arguments);
  }

}
