<?php

namespace Drupal\api_toolkit\Normalizer\Placeholder;

/**
 * Represents a type of argument to be passed to a placeholder callback.
 */
enum PlaceholderArgumentType {

  case Value;
  case Array;
  case Entity;
  case FieldItem;
  case FieldItemList;

}
