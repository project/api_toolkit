<?php

namespace Drupal\api_toolkit\Normalizer\Placeholder;

/**
 * Represents an argument to be passed to a placeholder callback.
 */
class PlaceholderArgument {

  public function __construct(
    public PlaceholderArgumentType $type,
    public mixed $value,
  ) {
  }

}
