<?php

namespace Drupal\api_toolkit\Normalizer;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\api_toolkit\Cache\CacheableMetadataWithKeys;
use Drupal\api_toolkit\Normalizer\Placeholder\Placeholder;
use Drupal\api_toolkit\Normalizer\Placeholder\PlaceholderArgument;
use Drupal\api_toolkit\Normalizer\Placeholder\PlaceholderArgumentType;
use Symfony\Component\Serializer\Serializer;

/**
 * Normalizes objects and caches the result.
 *
 * @see \Drupal\serialization\Normalizer\CacheableNormalizerInterface
 * @see \Drupal\rest\EventSubscriber\ResourceResponseSubscriber::renderResponseBody()
 */
class CachedNormalizer extends Serializer {

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The cache contexts manager.
   *
   * @var \Drupal\Core\Cache\Context\CacheContextsManager
   */
  protected $cacheContextsManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * CachedNormalizer constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \Drupal\Core\Cache\Context\CacheContextsManager $cacheContextsManager
   *   The cache contexts manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param array<NormalizerInterface|DenormalizerInterface> $normalizers
   *   The registered normalizers.
   * @param array<EncoderInterface|DecoderInterface> $encoders
   *   The registered encoders.
   */
  public function __construct(
    CacheBackendInterface $cache,
    CacheContextsManager $cacheContextsManager,
    EntityTypeManagerInterface $entityTypeManager,
    array $normalizers = [],
    array $encoders = [],
  ) {
    parent::__construct($normalizers, $encoders);
    $this->cache = $cache;
    $this->cacheContextsManager = $cacheContextsManager;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize(mixed $data, mixed $format = NULL, array $context = []): array|string|int|float|bool|\ArrayObject|null {
    $cacheability = $context['cacheability'] ??= new CacheableMetadata();

    if (is_array($data) || $data instanceof \Iterator) {
      $normalized = [];
      $originalCacheability = $cacheability;

      foreach ($data as $key => $val) {
        $subCacheability = clone $originalCacheability;
        $normalized[$key] = $this->normalize($val, $format, ['cacheability' => $subCacheability] + $context);
        $context['cacheability']->addCacheableDependency($subCacheability);
      }

      return $normalized;
    }

    $cid = $this->createCacheId($data, $cacheability, $format, $context);
    if ($cid === NULL) {
      return parent::normalize($data, $format, $context);
    }

    // If the maximum age is zero, then caching is effectively prohibited.
    if ($cacheability->getCacheMaxAge() === 0) {
      $normalization = parent::normalize($data, $format, $context);
      return $this->replacePlaceholders($normalization);
    }

    if ($cache = $this->cache->get($cid)) {
      $data = $cache->data['data'];
      $metadata = CacheableMetadata::createFromRenderArray($cache->data['cacheability']);
      $context['cacheability']->addCacheableDependency($metadata);

      return $this->replacePlaceholders($data);
    }

    $normalization = parent::normalize($data, $format, $context);
    $result = [
      'data' => $normalization,
      'cacheability' => [],
    ];

    if (is_array($result['data'])) {
      $result['data'] = $this->normalizePlaceholders($result['data']);
    }

    $cacheability->applyTo($result['cacheability']);
    $this->cache->set($cid, $result, $cacheability->getCacheMaxAge(), $cacheability->getCacheTags());

    return $this->replacePlaceholders($normalization);
  }

  /**
   * Replaces any placeholders in the normalization result.
   */
  protected function replacePlaceholders(mixed $normalization): mixed {
    if (!is_array($normalization)) {
      return $normalization;
    }

    foreach ($normalization as $key => $value) {
      if (is_array($value)) {
        $normalization[$key] = $this->replacePlaceholders($value);
        continue;
      }

      if ($value instanceof Placeholder) {
        $value->arguments = $this->denormalizeArguments($value->arguments);
        $normalization[$key] = $value();
      }
    }

    return $normalization;
  }

  /**
   * Normalizes any placeholders in the normalization result before storage.
   */
  protected function normalizePlaceholders(array $normalization): array {
    foreach ($normalization as $key => $value) {
      if (is_array($value)) {
        $normalization[$key] = $this->normalizePlaceholders($value);
      }
      if ($value instanceof Placeholder) {
        $value->arguments = $this->normalizeArguments($value->arguments);
      }
    }

    return $normalization;
  }

  /**
   * Normalizes any placeholder arguments before storage.
   *
   * @param array $arguments
   *   The arguments.
   *
   * @return \Drupal\api_toolkit\Normalizer\Placeholder\PlaceholderArgument[]
   *   The normalized arguments.
   */
  protected function normalizeArguments(array $arguments): array {
    foreach ($arguments as &$argument) {
      if (is_array($argument)) {
        $argument = new PlaceholderArgument(
          type: PlaceholderArgumentType::Array,
          value: $this->normalizeArguments($argument),
        );
        continue;
      }

      if ($argument instanceof EntityInterface) {
        $argument = new PlaceholderArgument(
          type: PlaceholderArgumentType::Entity,
          value: [
            $argument->getEntityTypeId(),
            $argument->id(),
            $argument->language()->getId(),
          ],
        );
        continue;
      }

      if ($argument instanceof FieldItemListInterface) {
        $argument = new PlaceholderArgument(
          type: PlaceholderArgumentType::FieldItemList,
          value: [
            $argument->getEntity()->getEntityTypeId(),
            $argument->getEntity()->id(),
            $argument->getLangcode(),
            $argument->getFieldDefinition()->getName(),
          ],
        );
        continue;
      }

      if ($argument instanceof FieldItemInterface) {
        $argument = new PlaceholderArgument(
          type: PlaceholderArgumentType::FieldItem,
          value: [
            $argument->getEntity()->getEntityTypeId(),
            $argument->getEntity()->id(),
            $argument->getLangcode(),
            $argument->getFieldDefinition()->getName(),
            $argument->getName(),
          ],
        );
        continue;
      }

      $argument = new PlaceholderArgument(
        type: PlaceholderArgumentType::Value,
        value: $argument,
      );
    }

    return $arguments;
  }

  /**
   * Denormalizes any placeholder arguments before replacement.
   *
   * @param \Drupal\api_toolkit\Normalizer\Placeholder\PlaceholderArgument[] $arguments
   *   The normalized arguments.
   *
   * @return array
   *   The denormalized arguments.
   */
  protected function denormalizeArguments(array $arguments): array {
    foreach ($arguments as &$argument) {
      if ($argument->type === PlaceholderArgumentType::Array) {
        $argument = $this->denormalizeArguments($argument->value);
        continue;
      }

      if ($argument->type === PlaceholderArgumentType::Value) {
        $argument = $argument->value;
        continue;
      }

      $entity = $this->entityTypeManager
        ->getStorage($argument->value[0])
        ->load($argument->value[1]);

      if ($entity instanceof ContentEntityInterface && $entity->hasTranslation($argument->value[2])) {
        $entity = $entity->getTranslation($argument->value[2]);
      }

      if ($argument->type === PlaceholderArgumentType::Entity) {
        $argument = $entity;
        continue;
      }

      if ($argument->type === PlaceholderArgumentType::FieldItemList) {
        $fieldItemList = NULL;
        if ($entity instanceof ContentEntityInterface) {
          $fieldItemList = $entity->get($argument->value[3]);
        }

        $argument = $fieldItemList;
        continue;
      }

      if ($argument->type === PlaceholderArgumentType::FieldItem) {
        $fieldItem = NULL;
        if ($entity instanceof ContentEntityInterface) {
          $fieldItem = $entity->get($argument->value[3])->get($argument->value[4]);
        }

        $argument = $fieldItem;
      }
    }

    return $arguments;
  }

  /**
   * Creates the cache ID string based on cache contexts.
   *
   * @param mixed $data
   *   The data to be normalized.
   * @param \Drupal\Core\Cache\CacheableDependencyInterface $cacheability
   *   The cacheability metadata.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The serialization context.
   *
   * @return string|null
   *   The cache ID string, or NULL if the element may not be cached.
   */
  protected function createCacheId($data, CacheableDependencyInterface $cacheability, ?string $format, array $context): ?string {
    $cacheKeys = $this->getCacheKeys($data, $cacheability, $format, $context);
    return implode(':', $cacheKeys) ?: NULL;
  }

  /**
   * Attempt to get cache keys from the data that will be normalized.
   *
   * @param mixed $data
   *   The data to be normalized.
   * @param \Drupal\Core\Cache\CacheableDependencyInterface $cacheability
   *   The cacheability metadata.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The serialization context.
   *
   * @return string[]
   *   The cache keys.
   */
  protected function getCacheKeys($data, CacheableDependencyInterface $cacheability, ?string $format, array $context): array {
    $cacheKeys = [];

    if ($data instanceof EntityInterface) {
      $cacheKeys = array_merge($cacheKeys, [
        'entity',
        $data->getEntityTypeId(),
        $data->id(),
        $data->language()->getId(),
      ]);
    }

    if ($data instanceof FieldItemListInterface) {
      $cacheKeys = array_merge($cacheKeys, [
        'field_item_list',
        $data->getEntity()->getEntityTypeId(),
        $data->getEntity()->id(),
        $data->getFieldDefinition()->getName(),
        $data->getLangcode(),
      ]);
    }

    if ($data instanceof FieldItemInterface) {
      $cacheKeys = array_merge($cacheKeys, [
        'field_item',
        $data->getEntity()->getEntityTypeId(),
        $data->getEntity()->id(),
        $data->getFieldDefinition()->getName(),
        $data->getLangcode(),
        $data->getName(),
      ]);
    }

    if ($cacheability instanceof CacheableMetadataWithKeys) {
      $metadataCacheKeys = $cacheability->getCacheKeys();
      $cacheKeys = array_merge($cacheKeys, $metadataCacheKeys);
    }

    if ($cacheKeys === []) {
      return [];
    }

    if ($contexts = $cacheability->getCacheContexts()) {
      $contextCacheKeys = $this->cacheContextsManager->convertTokensToKeys($contexts)->getKeys();
      sort($contextCacheKeys);
      $cacheKeys = array_merge($cacheKeys, $contextCacheKeys);
    }

    if (is_string($format)) {
      $cacheKeys[] = 'format';
      $cacheKeys[] = $format;
    }

    foreach ($context as $key => $value) {
      if (is_scalar($value)) {
        $cacheKeys[] = (string) $key;
        if (is_bool($value)) {
          $cacheKeys[] = $value ? 'true' : 'false';
        }
        else {
          $cacheKeys[] = (string) $value;
        }
      }
    }

    return $cacheKeys;
  }

}
