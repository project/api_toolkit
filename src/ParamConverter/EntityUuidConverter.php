<?php

namespace Drupal\api_toolkit\ParamConverter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\ParamConverter\EntityConverter;
use Symfony\Component\Routing\Route;

/**
 * Parameter converter for upcasting entity UUIDs to full objects.
 */
class EntityUuidConverter extends EntityConverter {

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    $entity_type_id = $this->getEntityTypeFromDefaults($definition, $name, $defaults);

    try {
      $entity = $this->entityRepository->loadEntityByUuid($entity_type_id, $value);
    }
    catch (EntityStorageException $exception) {
      return NULL;
    }

    if (
      !empty($definition['bundle']) &&
      $entity instanceof EntityInterface &&
      !in_array($entity->bundle(), $definition['bundle'], TRUE)
    ) {
      return NULL;
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route): bool {
    if (!empty($definition['type']) && str_starts_with($definition['type'], 'entity_uuid:')) {
      $entityTypeId = substr($definition['type'], strlen('entity_uuid:'));

      if (str_contains($definition['type'], '{')) {
        $entityTypeSlug = substr($entityTypeId, 1, -1);
        return $name != $entityTypeSlug && in_array($entityTypeSlug, $route->compile()->getVariables(), TRUE);
      }

      return $this->entityTypeManager->hasDefinition($entityTypeId);
    }

    return FALSE;
  }

}
