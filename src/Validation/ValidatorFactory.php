<?php

namespace Drupal\api_toolkit\Validation;

use Doctrine\Common\Annotations\AnnotationReader;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Validation\ConstraintValidatorFactory;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Factory for creating validators.
 */
class ValidatorFactory {

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * Constructs a new ValidatorFactory.
   *
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   A class resolver.
   */
  public function __construct(ClassResolverInterface $class_resolver) {
    $this->classResolver = $class_resolver;
  }

  /**
   * Creates a validator.
   */
  public function createValidator(): ValidatorInterface {
    // Prevent 'The annotation "@cache_contexts_manager" in method
    // Drupal\Core\Cache\CacheableDependencyInterface::getCacheContexts()
    // was never imported.'.
    AnnotationReader::addGlobalIgnoredName('cache_contexts_manager');

    $builder = Validation::createValidatorBuilder()
      ->setConstraintValidatorFactory(new ConstraintValidatorFactory($this->classResolver));

    // The enableAnnotationMapping() method is deprecated since Symfony 6.4.
    if (method_exists($builder, 'enableAttributeMapping')) {
      $builder->enableAttributeMapping();
    }
    else {
      $builder->enableAnnotationMapping();
    }

    // Since Symfony 6, ValidatorBuilder::enableAnnotationMapping() won't
    // automatically set up a Doctrine annotation reader anymore.
    if (method_exists($builder, 'addDefaultDoctrineAnnotationReader')) {
      $builder->addDefaultDoctrineAnnotationReader();
    }

    return $builder->getValidator();
  }

}
