<?php

namespace Drupal\api_toolkit\EventSubscriber;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\ParamConverter\ParamNotConvertedException;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Site\MaintenanceModeInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * Maintenance mode subscriber to return a JSON response.
 */
class MaintenanceModeSubscriber implements EventSubscriberInterface {

  /**
   * The maintenance mode.
   *
   * @var \Drupal\Core\Site\MaintenanceMode
   */
  protected $maintenanceMode;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The router.
   *
   * @var \Symfony\Component\Routing\RouterInterface
   */
  protected $router;

  /**
   * Constructs a new MaintenanceModeSubscriber.
   *
   * @param \Drupal\Core\Site\MaintenanceModeInterface $maintenanceMode
   *   The maintenance mode.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Symfony\Component\Routing\RouterInterface $router
   *   The router.
   */
  public function __construct(
    MaintenanceModeInterface $maintenanceMode,
    ConfigFactoryInterface $configFactory,
    RouterInterface $router,
  ) {
    $this->maintenanceMode = $maintenanceMode;
    $this->configFactory = $configFactory;
    $this->router = $router;
  }

  /**
   * Return an error response if site is in maintenance mode.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event to process.
   */
  public function onKernelRequestMaintenance(RequestEvent $event): void {
    $request = $event->getRequest();
    $routeMatch = $this->getRouteMatch($request);

    if (!$this->isApiToolkitRequest($routeMatch)) {
      return;
    }
    if (!$this->isJsonRequest($request)) {
      return;
    }

    if ($this->maintenanceMode->applies($routeMatch)) {
      $response = new JsonResponse([
        'errors' => [
          ['message' => $this->getSiteMaintenanceMessage()],
        ],
      ], Response::HTTP_SERVICE_UNAVAILABLE);
      $event->setResponse($response);
    }
  }

  /**
   * Gets the site maintenance message.
   *
   * @return \Drupal\Component\Render\FormattableMarkup
   *   The formatted site maintenance message.
   */
  protected function getSiteMaintenanceMessage(): FormattableMarkup {
    return new FormattableMarkup($this->configFactory->get('system.maintenance')->get('message'), [
      '@site' => $this->configFactory->get('system.site')->get('name'),
    ]);
  }

  /**
   * Determines whether a request accepts a JSON response.
   */
  protected function isJsonRequest(Request $request): bool {
    return in_array('application/json', $request->getAcceptableContentTypes(), TRUE);
  }

  /**
   * Determines whether a request is allowed to be altered by this module.
   */
  protected function isApiToolkitRequest(RouteMatchInterface $routeMatch): bool {
    $settings = $this->configFactory->get('api_toolkit.settings');
    $route = $routeMatch->getRouteObject();

    if (!$route instanceof Route) {
      // We don't know what route this is, so do nothing.
      return FALSE;
    }

    $format = $route->getRequirement('_format');

    return is_string($format) && in_array($format, $settings->get('route_formats'), TRUE);
  }

  /**
   * Get a route match based on the passed request.
   *
   * Matches the request through the router if necessary.
   */
  protected function getRouteMatch(Request $request): RouteMatchInterface {
    $routeMatch = RouteMatch::createFromRequest($request);
    if ($routeMatch instanceof RouteMatch) {
      return $routeMatch;
    }

    try {
      // If this is an ExceptionEvent, no route match will be available.
      // Let's do the route matching ourselves.
      $request = clone $request;
      $request->attributes->add($this->router->matchRequest($request));
    }
    catch (ParamNotConvertedException $e) {
      // @ignoreException
    }
    catch (ResourceNotFoundException $e) {
      // @ignoreException
    }
    catch (MethodNotAllowedException $e) {
      // @ignoreException
    }
    catch (AccessDeniedHttpException $e) {
      // @ignoreException
    }

    return RouteMatch::createFromRequest($request);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::REQUEST][] = ['onKernelRequestMaintenance', 31];
    $events[KernelEvents::EXCEPTION][] = ['onKernelRequestMaintenance', 1];
    return $events;
  }

}
