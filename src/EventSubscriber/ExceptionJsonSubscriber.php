<?php

namespace Drupal\api_toolkit\EventSubscriber;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\EventSubscriber\HttpExceptionSubscriberBase;
use Drupal\Core\ParamConverter\ParamNotConvertedException;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\api_toolkit\Exception\ApiValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Handles json error responses with validation results in a standardised way.
 */
class ExceptionJsonSubscriber extends HttpExceptionSubscriberBase {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The router.
   *
   * @var \Symfony\Component\Routing\RouterInterface
   */
  protected $router;

  /**
   * Constructs a new ExceptionJsonSubscriber.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Symfony\Component\Routing\RouterInterface $router
   *   The router.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    RouterInterface $router,
  ) {
    $this->configFactory = $configFactory;
    $this->router = $router;
  }

  /**
   * {@inheritdoc}
   */
  protected function getHandledFormats(): array {
    return ['json'];
  }

  /**
   * {@inheritdoc}
   */
  protected static function getPriority(): int {
    return -40;
  }

  /**
   * {@inheritdoc}
   */
  public function onException(ExceptionEvent $event): void {
    $request = $event->getRequest();
    $routeName = $this->getRouteName($request);

    if ($routeName === NULL) {
      return;
    }
    if (!$this->isApiToolkitRequest($routeName)) {
      return;
    }

    $exception = $event->getThrowable();
    if ($exception instanceof ApiValidationException) {
      $violations = $exception->getViolations();
      $response = $this->createJsonResponse($exception, $violations);
      $event->setResponse($response);

      return;
    }

    $message = $this->getMessage($exception);
    $violations = ConstraintViolationList::createFromMessage($message);
    $response = $this->createJsonResponse($exception, $violations);
    $event->setResponse($response);
  }

  /**
   * Build a general error message in case no validation errors are present.
   */
  protected function getMessage(\Throwable $exception): string {
    $message = $exception->getMessage();

    if ($message && error_displayable()) {
      return $message;
    }

    if ($exception instanceof HttpExceptionInterface) {
      $status = $exception->getStatusCode();

      if ($status === Response::HTTP_NOT_FOUND) {
        return $this->t('The requested page could not be found.');
      }

      if ($status === Response::HTTP_FORBIDDEN) {
        return $this->t('You are not authorized to access this page.');
      }

      if ($status === Response::HTTP_UNAUTHORIZED) {
        return $this->t('Please log in to access this page.');
      }

      if (substr($exception->getStatusCode(), 0, 1) === '4') {
        return $this->t('A client error happened');
      }
    }

    return $this->t('The website encountered an unexpected error. Please try again later.');
  }

  /**
   * Build a standardised JSON response based on a list of validation errors.
   */
  protected function createJsonResponse(\Throwable $exception, ConstraintViolationListInterface $violations): JsonResponse {
    $status = Response::HTTP_BAD_REQUEST;
    $headers = [];
    $data = [];

    /** @var \Symfony\Component\Validator\ConstraintViolationInterface $violation */
    foreach ($violations as $violation) {
      $error = [];
      if ($path = $violation->getPropertyPath()) {
        $error['path'] = $path;
      }
      if ($code = $violation->getCode()) {
        $error['code'] = $code;
      }
      $error['message'] = $violation->getMessage();

      $data['errors'][] = $error;
    }

    if ($exception instanceof HttpExceptionInterface) {
      $status = $exception->getStatusCode();
      $headers = $exception->getHeaders();
    }

    if (!isset($headers['Content-Type'])) {
      $headers['Content-Type'] = 'application/json';
    }

    // If the exception is cacheable, generate a cacheable response.
    if ($exception instanceof CacheableDependencyInterface) {
      $response = new CacheableJsonResponse($data, $status, $headers);
      $response->addCacheableDependency($exception);

      return $response;
    }

    return new JsonResponse($data, $status, $headers);
  }

  /**
   * Determines whether a request is allowed to be altered by this module.
   */
  protected function isApiToolkitRequest(string $routeName): bool {
    $settings = $this->configFactory->get('api_toolkit.settings');
    $route = $this->router->getRouteCollection()->get($routeName);

    if (!$route instanceof Route) {
      // We don't know what route this is, so do nothing.
      return FALSE;
    }

    $format = $route->getRequirement('_format');

    return is_string($format) && in_array($format, $settings->get('route_formats'), TRUE);
  }

  /**
   * Get a route match based on the passed request.
   *
   * Matches the request through the router if necessary.
   */
  protected function getRouteName(Request $request): ?string {
    $routeMatch = RouteMatch::createFromRequest($request);
    if ($routeMatch instanceof RouteMatch) {
      return $routeMatch->getRouteName();
    }

    try {
      // If this is an ExceptionEvent, no route match will be available.
      // Let's do the route matching ourselves.
      $request = clone $request;
      $request->attributes->add($this->router->matchRequest($request));
    }
    catch (ParamNotConvertedException $e) {
      return $e->getRouteName();
    }
    catch (ResourceNotFoundException $e) {
      // @ignoreException
    }
    catch (MethodNotAllowedException $e) {
      // @ignoreException
    }
    catch (AccessDeniedHttpException $e) {
      // @ignoreException
    }

    return RouteMatch::createFromRequest($request)->getRouteName();
  }

}
