<?php

namespace Drupal\api_toolkit\Response;

use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Cache\CacheableResponseTrait;

/**
 * A cacheable JSON response.
 */
class CacheableJsonResponse extends JsonResponse implements CacheableResponseInterface {

  use CacheableResponseTrait;

}
