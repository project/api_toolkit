<?php

namespace Drupal\api_toolkit\Response;

use Symfony\Component\HttpFoundation\JsonResponse as JsonResponseBase;

/**
 * A JSON response with a standardised structure.
 */
class JsonResponse extends JsonResponseBase {

  /**
   * Create a JSON response with an array of items under the 'data' key.
   */
  public static function createWithData(array $data, int $status = 200, array $headers = []): static {
    return new static(['data' => $data], $status, $headers);
  }

}
