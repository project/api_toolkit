<?php

namespace Drupal\api_toolkit\Response;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * An extension of the standard JSON response containing a violation list.
 */
class ApiErrorJsonResponse extends JsonResponse {

  /**
   * The violation list.
   *
   * @var \Symfony\Component\Validator\ConstraintViolationListInterface|null
   */
  protected $violations;

  /**
   * Constructs a new ApiErrorJsonResponse.
   *
   * @param \Symfony\Component\Validator\ConstraintViolationListInterface $violations
   *   A violation list.
   * @param int $status
   *   The HTTP status code that should be returned in the response.
   * @param array $headers
   *   Any HTTP headers that should be included in the response.
   * @param bool $json
   *   If the data is already a JSON string.
   */
  public function __construct(ConstraintViolationListInterface $violations, int $status = Response::HTTP_BAD_REQUEST, array $headers = [], bool $json = FALSE) {
    $this->violations = $violations;
    parent::__construct(NULL, $status, $headers, $json);
  }

  /**
   * Factory method for creating an instance with a violation list.
   */
  public static function createWithViolations(ConstraintViolationListInterface $violations, int $status = Response::HTTP_BAD_REQUEST, array $headers = [], bool $json = FALSE): static {
    return new static($violations, $status, $headers, $json);
  }

  /**
   * Add a violation.
   */
  public function addViolation(ConstraintViolationInterface $violation): self {
    $this->violations->add($violation);
    return $this;
  }

  /**
   * Add multiple violations.
   */
  public function addViolations(ConstraintViolationListInterface $violations): self {
    $this->violations->addAll($violations);
    return $this;
  }

  /**
   * Get all violations.
   */
  public function getViolations(): ConstraintViolationListInterface {
    return $this->violations;
  }

  /**
   * {@inheritdoc}
   */
  protected function update(): static {
    return $this->setContent([
      'errors' => $this->violations,
    ]);
  }

}
