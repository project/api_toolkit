<?php

namespace Drupal\api_toolkit\Response;

use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Cache\CacheableResponseTrait;
use Drupal\Core\Pager\Pager;

/**
 * A cacheable JSON response with a standardised structure and pagination info.
 */
class CacheablePagedJsonResponse extends PagedJsonResponse implements CacheableResponseInterface {

  use CacheableResponseTrait;

  /**
   * {@inheritdoc}
   */
  public static function createWithPager(array $data, Pager $pager, int $status = 200, array $headers = []): static {
    $response = parent::createWithPager($data, $pager, $status, $headers);
    $response->getCacheableMetadata()
      ->addCacheContexts(['url.query_args']);

    return $response;
  }

}
