<?php

namespace Drupal\api_toolkit\Response;

use Drupal\Core\Pager\Pager;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * A JSON response with a standardised structure and pagination info.
 */
class PagedJsonResponse extends JsonResponse {

  /**
   * Create a JSON response.
   *
   * Contains an array of items under the 'data' key
   * and an array of pagination info under the 'pagination' key.
   */
  public static function createWithPager(array $data, Pager $pager, int $status = 200, array $headers = []): static {
    $queryParams = \Drupal::request()->query->all();
    $data = [
      'pagination' => [
        'currentPage' => $pager->getCurrentPage(),
        'totalPages' => $pager->getTotalPages(),
        'totalItems' => (int) $pager->getTotalItems(),
        'limit' => $pager->getLimit(),
      ],
      'data' => $data,
      'links' => [],
    ];

    if ($pager->getCurrentPage() > 0) {
      $data['links']['prev'] = Url::fromRoute('<current>', ['page' => $pager->getCurrentPage() - 1] + $queryParams)
        ->toString(TRUE)
        ->getGeneratedUrl();
    }

    if (($pager->getCurrentPage() + 1) < $pager->getTotalPages()) {
      $data['links']['next'] = Url::fromRoute('<current>', ['page' => $pager->getCurrentPage() + 1] + $queryParams)
        ->toString(TRUE)
        ->getGeneratedUrl();
    }

    return new static($data, $status, $headers);
  }

}
