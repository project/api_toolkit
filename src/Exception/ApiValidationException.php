<?php

namespace Drupal\api_toolkit\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * An exception that can be thrown in case of a failed validation.
 */
class ApiValidationException extends HttpException {

  /**
   * The violation list.
   *
   * @var \Symfony\Component\Validator\ConstraintViolationListInterface|null
   */
  protected $violations;

  /**
   * Constructs a new ApiValidationException.
   *
   * @param \Symfony\Component\Validator\ConstraintViolationListInterface|null $violations
   *   A violation list.
   * @param int $status
   *   The HTTP status code that should be returned in the response.
   * @param string|null $message
   *   The Exception message to throw.
   * @param \Throwable|null $previous
   *   The previous throwable used for the exception chaining.
   * @param array $headers
   *   Any HTTP headers that should be included in the response.
   * @param string|null $code
   *   The violation code.
   * @param string|null $path
   *   The property path.
   */
  public function __construct(?ConstraintViolationListInterface $violations = NULL, int $status = Response::HTTP_BAD_REQUEST, ?string $message = '', ?\Throwable $previous = NULL, array $headers = [], ?string $code = NULL, ?string $path = NULL) {
    $this->violations = $violations;
    $this->ensureViolationList();

    if ($message) {
      $this->violations->add(new ConstraintViolation($message, $message, [], '', $path, NULL, NULL, $code));
    }

    parent::__construct($status, $message, $previous, $headers);
    $this->updateMessage();
  }

  /**
   * Create a new exception.
   */
  public static function create(?ConstraintViolationListInterface $violations = NULL, int $statusCode = Response::HTTP_BAD_REQUEST, ?string $message = '', ?\Throwable $previous = NULL, array $headers = [], ?string $code = NULL, ?string $path = NULL) {
    return new static($violations, $statusCode, $message, $previous, $headers, $code, $path);
  }

  /**
   * Add a violation.
   */
  public function addViolation(ConstraintViolationInterface $violation): self {
    $this->ensureViolationList();
    $this->violations->add($violation);
    $this->updateMessage();
    return $this;
  }

  /**
   * Add multiple violations.
   */
  public function addViolations(ConstraintViolationListInterface $violations): self {
    $this->ensureViolationList();
    $this->violations->addAll($violations);
    $this->updateMessage();
    return $this;
  }

  /**
   * Get all violations.
   */
  public function getViolations(): ?ConstraintViolationListInterface {
    return $this->violations;
  }

  /**
   * Create a violation list if none exists yet.
   */
  protected function ensureViolationList(): void {
    if (!isset($this->violations)) {
      $this->violations = new ConstraintViolationList();
    }
  }

  /**
   * Update exception message in order to provide more context in error logs.
   */
  protected function updateMessage(): void {
    $messages = [];
    foreach ($this->getViolations() ?? [] as $violation) {
      if ($path = $violation->getPropertyPath()) {
        $messages[] = sprintf('%s (at path %s)', $violation->getMessage(), $path);
      }
      else {
        $messages[] = $violation->getMessage();
      }
    }
    $this->message = implode(', ', $messages);
  }

}
