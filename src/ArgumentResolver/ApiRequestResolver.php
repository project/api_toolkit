<?php

namespace Drupal\api_toolkit\ArgumentResolver;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\api_toolkit\Exception\ApiValidationException;
use Drupal\api_toolkit\Request\ApiRequestInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Routing\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Converts normal Symfony requests to ApiRequestInterface instances.
 */
class ApiRequestResolver implements ValueResolverInterface {

  /**
   * The Symfony normalizer.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface
   */
  protected $normalizer;

  /**
   * The Symfony validator.
   *
   * @var \Symfony\Component\Validator\Validator\ValidatorInterface
   */
  protected $validator;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new ApiRequestResolver object.
   *
   * @param \Symfony\Component\Serializer\Normalizer\NormalizerInterface $normalizer
   *   The Symfony normalizer.
   * @param \Symfony\Component\Validator\Validator\ValidatorInterface $validator
   *   The Symfony validator.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(
    NormalizerInterface $normalizer,
    ValidatorInterface $validator,
    ConfigFactoryInterface $configFactory,
  ) {
    $this->normalizer = $normalizer;
    $this->validator = $validator;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function resolve(Request $request, ArgumentMetadata $argument): iterable {
    if (!is_a($argument->getType(), ApiRequestInterface::class, TRUE)) {
      return;
    }

    /** @var class-string<ApiRequestInterface> $requestClass */
    $requestClass = $argument->getType();

    // Transform to API request class.
    $apiRequest = $this->normalizer->denormalize($request, $requestClass);

    // Validate the request.
    $route = $request->attributes->get('_route_object');
    assert($route instanceof Route);

    if ($this->shouldValidate($route)) {
      $groups = NULL;
      if ($route->hasOption('_api_validation_groups')) {
        $groups = (array) $route->getOption('_api_validation_groups');
      }

      $violations = $this->validator->validate($apiRequest, NULL, $groups);
      if ($violations->count() > 0) {
        throw ApiValidationException::create($violations, Response::HTTP_UNPROCESSABLE_ENTITY);
      }
    }

    yield $apiRequest;
  }

  /**
   * Determines if the request should be validated.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route object.
   *
   * @return bool
   *   TRUE if the request should be validated, FALSE otherwise.
   */
  protected function shouldValidate(Route $route): bool {
    if ($route->hasOption('_api_validation')) {
      return $route->getOption('_api_validation');
    }

    $config = $this->configFactory->get('api_toolkit.settings');
    return $config->get('auto_validate');
  }

}
