<?php

namespace Drupal\api_toolkit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\Reference;

/**
 * The service provider.
 */
class ApiToolkitServiceProvider implements ServiceProviderInterface, ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    // Add a compiler pass for populating the CachedNormalizer with registered
    // normalizers and encoders.
    $container->addCompilerPass(new RegisterSerializationClassesCompilerPass(), PassConfig::TYPE_BEFORE_OPTIMIZATION, -10);
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {
    // Fixes error if the serializer module is not yet installed.
    if (!$container->hasDefinition('serializer')) {
      $container->removeDefinition('api_toolkit.argument_resolver.api_request');
      return;
    }

    $argumentResolver = $container->getDefinition('http_kernel.controller.argument_resolver');
    $argumentValueResolvers = $argumentResolver->getArgument(1);
    array_unshift($argumentValueResolvers, new Reference('api_toolkit.argument_resolver.api_request'));
    $argumentResolver->setArgument(1, $argumentValueResolvers);
  }

}
