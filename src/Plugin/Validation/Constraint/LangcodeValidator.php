<?php

namespace Drupal\api_toolkit\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Validates the Langcode constraint.
 */
class LangcodeValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->languageManager = $container->get('language_manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint): void {
    if (!$constraint instanceof Langcode) {
      throw new UnexpectedTypeException($constraint, Langcode::class);
    }

    if ($value !== NULL && !is_scalar($value) && !(\is_object($value) && method_exists($value, '__toString'))) {
      throw new UnexpectedTypeException($value, 'string');
    }

    if ($value === NULL || $value === '') {
      return;
    }

    if (!$this->languageManager->getLanguage($value)) {
      $this->context->buildViolation($constraint->message)
        ->setParameter('%value', $value)
        ->setCode(Langcode::INVALID_LANGCODE_ERROR)
        ->addViolation();
    }
  }

}
