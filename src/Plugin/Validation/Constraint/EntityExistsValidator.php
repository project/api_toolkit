<?php

namespace Drupal\api_toolkit\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Validates the EntityExists constraint.
 */
class EntityExistsValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityFieldManager = $container->get('entity_field.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint): void {
    if (!$constraint instanceof EntityExists) {
      throw new UnexpectedTypeException($constraint, EntityExists::class);
    }

    if ($value !== NULL && !is_scalar($value) && !(\is_object($value) && method_exists($value, '__toString'))) {
      throw new UnexpectedTypeException($value, 'string');
    }

    if ($value === NULL || $value === '') {
      return;
    }

    $entityType = $this->entityTypeManager->getDefinition($constraint->entityTypeId);
    $storage = $this->entityTypeManager->getStorage($constraint->entityTypeId);

    $entity = $storage->loadByProperties([$constraint->fieldName => $value]);
    $entity = reset($entity);

    if (!$entity instanceof EntityInterface || ($constraint->bundle && $entity->bundle() !== $constraint->bundle)) {
      $fieldDefinitions = $this->entityFieldManager->getFieldDefinitions($constraint->entityTypeId, $constraint->bundle ?? $constraint->entityTypeId);

      $this->context->buildViolation($constraint->message)
        ->setParameter('@entityType', $this->getEntityTypeLabel($entityType, $constraint->bundle))
        ->setParameter('%fieldLabel', mb_strtolower($fieldDefinitions[$constraint->fieldName]->getLabel()))
        ->setParameter('%value', $value)
        ->setCode(EntityExists::INVALID_ID_ERROR)
        ->addViolation();
    }
  }

  /**
   * Get the singular label of an entity type or bundle.
   */
  protected function getEntityTypeLabel(EntityTypeInterface $entityType, ?string $bundle): string {
    if ($bundle && $bundleEntityType = $entityType->getBundleEntityType()) {
      $bundleDefinition = $this->entityTypeManager
        ->getStorage($bundleEntityType)
        ->load($bundle);

      if ($bundleDefinition) {
        return strtolower($bundleDefinition->label());
      }
    }

    return $entityType->getSingularLabel();
  }

}
