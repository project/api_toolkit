<?php

namespace Drupal\api_toolkit\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Validates whether a certain source ID exists in the mapping of a migration.
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @Constraint(
 *     id = "migration_source_exists",
 *     label = @Translation("Migration source exists", context = "Validation"),
 * )
 */
#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
class MigrationSourceExists extends Constraint {

  public const INVALID_ID_ERROR = 'b678116c-6125-11ee-8c99-0242ac120002';
  public const DESTINATION_IDS_LOOKUP_FAILED_ERROR = '0a4220e7-740a-4492-93e8-b5e7f485c257';

  /**
   * {@inheritdoc}
   */
  protected static $errorNames = [
    self::INVALID_ID_ERROR => 'INVALID_ID_ERROR',
    self::DESTINATION_IDS_LOOKUP_FAILED_ERROR => 'DESTINATION_IDS_LOOKUP_FAILED_ERROR',
  ];

  /**
   * The migration id.
   *
   * @var string|null
   */
  public ?string $migration = NULL;

  /**
   * The error message in case of a non-existing source ID.
   *
   * @var string
   */
  public string $invalidIdMessage = 'No source with ID %value exists in migration %migration.';

  /**
   * The error message in case of a failed lookup of destination IDs.
   *
   * @var string
   */
  public string $destinationIdsLookupFailedMessage = 'Failed to lookup destination IDs for source ID %value in migration %migration: %exceptionMessage.';

  /**
   * {@inheritdoc}
   */
  public function getDefaultOption(): ?string {
    return 'migration';
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredOptions(): array {
    return ['migration'];
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(?array $options = NULL, ?string $migration = NULL, ?string $invalidIdMessage = NULL, ?string $destinationIdsLookupFailedMessage = NULL, ?array $groups = NULL, mixed $payload = NULL) {
    $options['migration'] ??= $migration;
    $options['invalidIdMessage'] ??= $invalidIdMessage ?? $this->invalidIdMessage;
    $options['destinationIdsLookupFailedMessage'] ??= $destinationIdsLookupFailedMessage ?? $this->destinationIdsLookupFailedMessage;

    parent::__construct($options, $groups, $payload);
  }

}
