<?php

namespace Drupal\api_toolkit\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Validates the Enum constraint.
 */
class EnumValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint): void {
    if (!$constraint instanceof Enum) {
      throw new UnexpectedTypeException($constraint, Enum::class);
    }

    if ($value !== NULL && !is_scalar($value) && !(\is_object($value) && method_exists($value, '__toString'))) {
      throw new UnexpectedTypeException($value, 'string');
    }

    if ($value === NULL || $value === '') {
      return;
    }

    assert(is_a($constraint->enum, \BackedEnum::class, TRUE), 'The Enum constraint must be used with a backed enum.');
    if (!$constraint->enum::tryFrom($value)) {
      $this->context->buildViolation($constraint->message)
        ->setParameter('%value', $value)
        ->setParameter('%cases', implode(', ', array_column($constraint->enum::cases(), 'value')))
        ->setCode(Enum::NO_SUCH_CASE_ERROR)
        ->addViolation();
    }
  }

}
