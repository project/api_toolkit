<?php

namespace Drupal\api_toolkit\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\migrate\MigrateException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Validates the MigrationSourceExists constraint.
 */
class MigrationSourceExistsValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The migration plugin manager.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected $migrationManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->migrationManager = $container->get('plugin.manager.migration');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint): void {
    if (!$constraint instanceof MigrationSourceExists) {
      throw new UnexpectedTypeException($constraint, MigrationSourceExists::class);
    }

    if ($value !== NULL && !is_scalar($value) && !(\is_object($value) && method_exists($value, '__toString'))) {
      throw new UnexpectedTypeException($value, 'string');
    }

    if ($value === NULL || $value === '') {
      return;
    }

    /** @var \Drupal\migrate\Plugin\MigrationInterface|false $migration */
    $migration = $this->migrationManager->createInstance($constraint->migration);
    if ($migration === FALSE) {
      $this->context->addViolation('The "%id" migration does not exist.', ['%id' => $constraint->migration]);
      return;
    }

    $idMap = $migration->getIdMap();

    try {
      $records = $idMap->lookupDestinationIds([$value]);
      if ($records === []) {
        $this->context->buildViolation($constraint->invalidIdMessage)
          ->setParameter('%value', $value)
          ->setParameter('%migration', $constraint->migration)
          ->setCode(MigrationSourceExists::INVALID_ID_ERROR)
          ->addViolation();
      }
    }
    catch (MigrateException $e) {
      $this->context->buildViolation($constraint->destinationIdsLookupFailedMessage)
        ->setParameter('%value', $value)
        ->setParameter('%migration', $constraint->migration)
        ->setParameter('%exceptionMessage', $e->getMessage())
        ->setCode(MigrationSourceExists::DESTINATION_IDS_LOOKUP_FAILED_ERROR)
        ->addViolation();
    }
  }

}
