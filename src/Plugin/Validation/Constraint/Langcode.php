<?php

namespace Drupal\api_toolkit\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Validates whether a certain value is a valid language code.
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @Constraint(
 *     id = "langcode",
 *     label = @Translation("Language code", context = "Validation"),
 * )
 */
#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
class Langcode extends Constraint {

  public const INVALID_LANGCODE_ERROR = 'f598446e-f8eb-4696-a35f-6a545087ae3b';

  /**
   * {@inheritdoc}
   */
  protected static $errorNames = [
    self::INVALID_LANGCODE_ERROR => 'INVALID_LANGCODE_ERROR',
  ];

  /**
   * The error message.
   *
   * @var string
   */
  public $message = '%value is not a valid language code.';

  /**
   * {@inheritdoc}
   */
  public function __construct(?array $options = NULL, ?string $message = NULL, ?array $groups = NULL, mixed $payload = NULL) {
    $options['message'] ??= $message ?? $this->message;

    parent::__construct($options, $groups, $payload);
  }

}
