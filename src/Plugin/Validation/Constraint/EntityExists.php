<?php

namespace Drupal\api_toolkit\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Validates whether an entity with a certain UUID exists.
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @Constraint(
 *     id = "entity_exists",
 *     label = @Translation("Checks if an entity with a certain id exists", context = "Validation"),
 * )
 */
#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
class EntityExists extends Constraint {

  public const INVALID_ID_ERROR = 'b678116c-6125-11ee-8c99-0242ac120002';

  /**
   * {@inheritdoc}
   */
  protected static $errorNames = [
    self::INVALID_ID_ERROR => 'INVALID_ID_ERROR',
  ];

  /**
   * The entity type ID.
   *
   * @var string|null
   */
  public ?string $entityTypeId = NULL;

  /**
   * The entity bundle.
   *
   * @var string|null
   */
  public ?string $bundle = NULL;

  /**
   * The field name to check.
   *
   * @var string
   */
  public string $fieldName = 'uuid';

  /**
   * The error message in case the identifier does not resolve to an entity.
   *
   * @var string
   */
  public string $message = 'No @entityType with %fieldLabel %value exists.';

  /**
   * {@inheritdoc}
   */
  public function getRequiredOptions(): array {
    return ['entityTypeId'];
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(?array $options = NULL, ?string $entityTypeId = NULL, ?string $bundle = NULL, ?string $fieldName = NULL, ?string $message = NULL, ?array $groups = NULL, mixed $payload = NULL) {
    $options['entityTypeId'] ??= $entityTypeId ?? $this->entityTypeId;
    $options['bundle'] ??= $bundle ?? $this->bundle;
    $options['fieldName'] ??= $fieldName ?? $this->fieldName;
    $options['message'] ??= $message ?? $this->message;

    parent::__construct($options, $groups, $payload);
  }

}
