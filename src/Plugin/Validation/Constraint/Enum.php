<?php

namespace Drupal\api_toolkit\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Choice;

/**
 * Validates whether an entity with a certain UUID exists.
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @Constraint(
 *     id = "enum",
 *     label = @Translation("Checks if an enum with a certain case exists", context = "Validation"),
 * )
 */
#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
class Enum extends Constraint {

  public const NO_SUCH_CASE_ERROR = Choice::NO_SUCH_CHOICE_ERROR;

  /**
   * {@inheritdoc}
   */
  protected static $errorNames = [
    self::NO_SUCH_CASE_ERROR => 'NO_SUCH_CASE_ERROR',
  ];

  /**
   * The enum.
   *
   * @var string|null
   */
  public ?string $enum = NULL;

  /**
   * The error message in case of an invalid enum case.
   *
   * @var string
   */
  public string $message = "The value '%value' is not a valid option. Choose one of the following: %cases.";

  /**
   * {@inheritdoc}
   */
  public function getDefaultOption(): ?string {
    return 'enum';
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredOptions(): array {
    return ['enum'];
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(?array $options = NULL, ?string $enum = NULL, ?string $message = NULL, ?array $groups = NULL, mixed $payload = NULL) {
    $options['enum'] ??= $enum;
    $options['message'] ??= $message ?? $this->message;

    parent::__construct($options, $groups, $payload);
  }

}
