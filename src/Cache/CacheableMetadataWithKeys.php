<?php

namespace Drupal\api_toolkit\Cache;

use Drupal\Core\Cache\CacheableMetadata;

/**
 * A value object to store cache keys with their cacheability metadata.
 *
 * @see \Drupal\Core\Cache\Context\ContextCacheKeys
 */
class CacheableMetadataWithKeys extends CacheableMetadata {

  /**
   * The cache keys.
   *
   * @var string[]
   */
  protected array $cacheKeys;

  /**
   * Constructs a CacheKeys object.
   *
   * @param string[] $keys
   *   The cache keys.
   */
  public function __construct(array $keys = []) {
    $this->cacheKeys = $keys;
    $this->sortCacheKeys();
  }

  /**
   * Gets the generated cache keys.
   *
   * @return string[]
   *   The cache keys.
   */
  public function getCacheKeys(): array {
    return $this->cacheKeys;
  }

  /**
   * Adds cache keys.
   *
   * @param string[] $keys
   *   The cache keys.
   */
  public function addCacheKeys(array $keys): void {
    $this->cacheKeys = array_merge($this->cacheKeys, $keys);
    $this->sortCacheKeys();
  }

  /**
   * Sorts the cache keys.
   *
   * Domain invariant: cache keys must be always sorted.
   * Sorting keys warrants that different combination of the same keys
   * generates the same cache cid.
   *
   * @see \Drupal\Core\Render\RenderCache::createCacheID()
   */
  protected function sortCacheKeys(): void {
    sort($this->cacheKeys);
  }

}
